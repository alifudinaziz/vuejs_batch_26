// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
for (var index = 0; index < daftarHewan.length; index++) {
  console.log(daftarHewan[index]);
}

// Soal 2
function introduce(name, age, address, hobby) {
  return (
    "Nama saya " +
    data.name +
    ", umur saya " +
    data.age +
    " tahun, alamat saya di " +
    data.address +
    ", dan saya punya hobby yaitu " +
    data.hobby +
    "!"
  );
}
var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};
var perkenalan = introduce(data);
console.log(perkenalan);

// Soal 3
function hitung_huruf_vokal(myString) {
  if (isNaN(myString)) {
    var kata = myString.match(/[aiueo]/gi);
    return kata === null ? 0 : kata.length;
  }
  return "Masukkan huruf";
}
var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");
console.log(hitung_1, hitung_2);

// Soal 4
function hitung(myInt) {
  if (isNaN(myInt)) {
    return "Masukkan angka";
  } else {
    if (myInt % 1 !== 0) {
      return Math.round(myInt) * 2 - 2;
    }
    return myInt * 2 - 2;
  }
}
console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(4));
console.log(hitung(5));
