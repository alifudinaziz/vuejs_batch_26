// Soal 1
function jumlah_kata(myKalimat) {
  var kata = myKalimat.split(" ");
  return kata.length;
}

var kalimat_1 = "Halo nama saya Alifudin Aziz";
var kalimat_2 = "Saya Aziz";
console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));

// Soal 2
function next_date(tanggal, bulan, tahun) {
  if (
    bulan == 1 ||
    bulan == 3 ||
    bulan == 5 ||
    bulan == 7 ||
    bulan == 8 ||
    bulan == 10
  ) {
    if (tanggal >= 31) {
      tanggal = 1;
      var intBulan = parseInt(bulan) + 1;
      bulan = intBulan;
    } else {
      tanggal += 1;
    }
  } else if (bulan == 2) {
    if (tahun % 4 == 0) {
      if (tanggal >= 29) {
        tanggal = 1;
        var intBulan = parseInt(bulan) + 1;
        bulan = intBulan;
      } else {
        tanggal += 1;
      }
    } else {
      if (tanggal >= 28) {
        tanggal = 1;
        var intBulan = parseInt(bulan) + 1;
        bulan = intBulan;
      } else {
        tanggal += 1;
      }
    }
  } else if (bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11) {
    if (tanggal >= 30) {
      tanggal = 1;
      var intBulan = parseInt(bulan) + 1;
      bulan = intBulan;
    } else {
      tanggal += 1;
    }
  } else if (bulan == 12) {
    if (tanggal >= 31) {
      tanggal = 1;
      bulan = 1;
      tahun += 1;
    } else {
      tanggal += 1;
    }
  } else {
    bulan = "Invalid";
  }
  switch (bulan) {
    case 1:
      bulan = "Januari";
      break;
    case 2:
      bulan = "Februari";
      break;
    case 3:
      bulan = "Maret";
      break;
    case 4:
      bulan = "April";
      break;
    case 5:
      bulan = "Mei";
      break;
    case 6:
      bulan = "Juni";
      break;
    case 7:
      bulan = "Juli";
      break;
    case 8:
      bulan = "Agustus";
      break;
    case 9:
      bulan = "September";
      break;
    case 10:
      bulan = "Oktober";
      break;
    case 11:
      bulan = "November";
      break;
    case 12:
      bulan = "Desember";
      break;
  }
  return tanggal + " " + bulan + " " + tahun;
}

var tanggal = 31;
var bulan = 12;
var tahun = 2020;
console.log(next_date(tanggal, bulan, tahun));
