// Soal 1
var nilai = 30;
if (nilai >= 85) {
  console.log("Indeks A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("Indeks B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("Indeks C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("Indeks D");
} else {
  console.log("Indeks E");
}

// Soal 2
var tanggal = 18;
var bulan = 6;
var tahun = 1997;
switch (bulan) {
  case 1:
    bulan = "Januari";
    break;
  case 2:
    bulan = "Februari";
    break;
  case 3:
    bulan = "Maret";
    break;
  case 4:
    bulan = "April";
    break;
  case 5:
    bulan = "Mei";
    break;
  case 6:
    bulan = "Juni";
    break;
  case 7:
    bulan = "Juli";
    break;
  case 8:
    bulan = "Agustus";
    break;
  case 9:
    bulan = "September";
    break;
  case 10:
    bulan = "Oktober";
    break;
  case 11:
    bulan = "November";
    break;
  case 12:
    bulan = "Desember";
    break;
  default:
    bulan = "Invalid";
    break;
}
console.log(tanggal + " " + bulan + " " + tahun);

// Soal 3
var input = 10;
var hasil = "";
for (var i = 0; i < input; i++) {
  for (var j = 0; j <= i; j++) {
    hasil += "#";
  }
  hasil += "\n";
}
console.log(hasil);

// Soal 4
var kalimat = "";
var m = 10;
var iterasiKalimat = 1;
var samadengan = "";
for (var i = 1; i <= m; i++) {
  switch (iterasiKalimat) {
    case 1:
      kalimat = "I love programming";
      break;
    case 2:
      kalimat = "I love Javascipt";
      break;
    case 3:
      kalimat = "I love VueJS";
      break;
  }
  if (iterasiKalimat == 3) {
    iterasiKalimat = 1;
  } else {
    iterasiKalimat++;
  }
  if (i % 3 == 0) {
    console.log(i + " - " + kalimat);
    for (let l = 1; l <= i; l++) {
      samadengan += "=";
    }
    console.log(samadengan);
    samadengan = "";
  } else {
    console.log(i + " - " + kalimat);
  }
}
