// export const MembersComponent = {
//   template: `
//             <div
//               class="col-md-3"
//               style="
//                 padding: 10px;
//                 margin: 10px;
//                 margin-left: auto;
//                 margin-right: auto;
//               "
//             >
//               <div class="card">
//                 <img
//                   v-bind:src="member.photo_profile ? domain + member.photo_profile : 'https://dummyimage.com/600x400/000/fff&text=image'"
//                   class="card-img-top"
//                   alt="image"
//                 />
//                 <div class="card-body">
//                   <h5 class="card-title">{{member.name}}</h5>
//                   <p class="card-text">{{member.address}}</p>
//                   <p class="card-text">{{member.no_hp}}</p>
//                 </div>
//                 <div class="card-footer">
//                   <button @click="$emit('edit', member)" class="btn btn-info">
//                     Edit
//                   </button>
//                   <button
//                     @click="$emit('delete', member.id)"
//                     class="btn btn-danger"
//                   >
//                     Hapus
//                   </button>
//                   <button
//                     @click="$emit('upload', member)"
//                     class="btn btn-secondary"
//                   >
//                     Upload Foto
//                   </button>
//                 </div>
//               </div>
//             </div>
//           </div>
//         `,
//   props: ["member", "domain"],
// };
