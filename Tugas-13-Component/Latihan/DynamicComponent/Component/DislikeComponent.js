export const DislikeComponent = {
  template: `
    <div class="card">
      <strong>Saya Tidak Suka</strong>
      <ul>
        <li>Gelut</li>
        <li>Eker</li>
        <li>Ngeyel</li>
      </ul>
    </div>`,
};
