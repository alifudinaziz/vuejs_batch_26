export const LikeComponent = {
  template: `
    <div class="card">
      <strong>Saya Suka</strong>
      <ul>
        <li>Mangan</li>
        <li>Turu</li>
        <li>Moco Manga</li>
      </ul>
    </div>`,
};
