export const FeaturesComponent = {
  template: `
  <div
    class="col-sm-6 col-md-3"
    style="
    padding: 10px;
    margin: 10px;
    margin-left: auto;
    margin-right: auto;
    "
    >
      <div class="card">
        <img src="https://i.pinimg.com/564x/83/c3/5b/83c35b46e547c7e758b00db0e7624534.jpg"
          class="card-img-top"
          alt="image"
        />
      <div class="card-body">
        <h5 class="card-title">MINI</h5>
        <p class="card-text">MINI Cooper S 2.0 141kW</p>
        <ul>
          <li><span>Engine Type</span> : 2.0L turbo</li>
          <li><span>Fuel Type</span> : Premium Unleaded Petrol</li>
          <li><span>Fuel Efficiency</span> : 6.4L/100km</li>
        </ul>
      </div>
      <div class="card-footer">
        <p class="card-text">2015</p>
      </div>
    </div>
  </div>`,
};
