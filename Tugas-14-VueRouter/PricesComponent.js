export const PricesComponent = {
  template: `
  <div
    class="col-sm-6 col-md-3"
    style="
    padding: 10px;
    margin: 10px;
    margin-left: auto;
    margin-right: auto;
    "
    >
      <div class="card">
        <img src="https://i.pinimg.com/564x/64/2c/c0/642cc011e0125439c3def378256ccde0.jpg"
          class="card-img-top"
          alt="image"
        />
      <div class="card-body">
        <h5 class="card-title">BMW</h5>
        <p class="card-text">BMW X2 M35i</p>
        <p class="card-text">Rp. 839.000.000,-</p>
      </div>
      <div class="card-footer">
        <p class="card-text">2019</p>
      </div>
    </div>
  </div>`,
};
