export const HomeComponent = {
  template: `
  <div
    class="col-sm-6 col-md-3"
    style="
    padding: 10px;
    margin: 10px;
    margin-left: auto;
    margin-right: auto;
    "
    >
      <div class="card">
        <img src="https://i.pinimg.com/564x/2e/40/02/2e40027b9b156589cfbccbf7b33d3bc7.jpg"
          class="card-img-top"
          alt="image"
        />
      <div class="card-body">
        <h5 class="card-title">Mercedes</h5>
        <p class="card-text">Mercedes-AMG GLE53 Coupe Electrifies</p>
      </div>
      <div class="card-footer">
        <p class="card-text">2021</p>
      </div>
    </div>
  </div>`,
};
