var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

function bacaBukunya(waktu, index) {
  if (index < books.length) {
    readBooksPromise(waktu, books[index]).then((sisa) => {
      if (sisa > 0) {
        index += 1;
        bacaBukunya(sisa, index);
      }
    });
    // let sisa = waktu - books[index].timeSpent;
  }
}

bacaBukunya(10000, 0);
