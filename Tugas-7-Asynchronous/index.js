var readBooks = require("./callback");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "Komik", timeSpent: 1000 },
];

function waktuBaca(waktu, bukuu, index) {
  if (index < books.length) {
    readBooks(waktu, books[index], (sisa) => {
      if (sisa > 0) {
        index += 1;
        waktuBaca(sisa, bukuu, index);
      }
    });
  }
}

waktuBaca(10000, books, 0);
