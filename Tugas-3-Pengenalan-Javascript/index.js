// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
console.log(
  pertama.substring(0, 4) +
    pertama.substring(11, 19) +
    kedua.substring(0, 7) +
    kedua.substring(7, 18).toUpperCase()
);

// Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var angkaPertama = parseInt(kataPertama);
var angkaKedua = parseInt(kataKedua);
var angkaKetiga = parseInt(kataKetiga);
var angkaKeempat = parseInt(kataKeempat);
console.log(
  angkaKeempat - angkaKetiga + angkaKedua + angkaPertama * angkaKedua
);

// Soal 3
var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 32);

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
