// Arrow Func
let p = 6;
let l = 4;
// Form 1
const luasFuncForm1 = (p, l) => {
  return p * l;
};
// Form 2 - No Arguments
const luasFuncForm2 = () => p * l;

console.log(luasFuncForm1(p, l));
console.log(luasFuncForm2());

// ==========================================
